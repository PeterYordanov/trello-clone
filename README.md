# Trello Clone

[[_TOC_]]

## CI/CD
[![pipeline status](https://gitlab.com/PeterYordanov/trello-clone/badges/master/pipeline.svg)](https://gitlab.com/PeterYordanov/trello-clone/commits/master)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)

![Recording](Recording.gif "Recording")

## Tech Stack
- React
- JavaScript

## System Design
```mermaid
graph TB
  subgraph "Trello Clone"
  CARD(Trello Card)
  ACTIONBUTTON(Trello Action Button)
  LIST(Card List)

  CARD --> LIST
  ACTIONBUTTON --> LIST
end
```

## Features
- [x] Add lists
- [x] Add cards
- [x] Draggable cards

