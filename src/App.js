import React, { Component } from 'react';
import './App.css';
import TrelloList from "./components/TrelloList"
import { connect } from "react-redux"
import TrelloActionButton from "./components/TrelloActionButton";
import { DragDropContext } from "react-beautiful-dnd";

class App extends Component {

  onDragEnd = () => {

  }

  render() {
    const { lists } = this.props;
    return (
      <DragDropContext onDragEnd={ this.onDragEnd }>
        <div className="App">
          <h2>Trello Clone</h2>
          <div style={ styles.listsContainer }>
              { 
                  lists.map(
                    list => <TrelloList listID={ list.id } key={ list.id } title={ list.title } cards={ list.cards } />
                  )
              }
              <TrelloActionButton list/>
          </div>
        </div>
      </DragDropContext>
    );
  }
}

const styles = {
  listsContainer: {
    display: "flex",
    flexDirection: "row",
    marginRight: 10
  }
}

const mapStateToProps = state => ({
  lists: state.lists
})

export default connect(mapStateToProps) (App);