import React from "react";
import Card from "@material-ui/core/Card";
import Typography from "@material-ui/core/Typography";
import { Draggable } from "react-beautiful-dnd"

const TrelloCard = ({ text, id, index }) => {
    return(
        <Draggable draggableId={ String(id) } index={ index }>
            { 
            provided => (
                <div ref={ provided.innerRef } 
                         { ...provided.draggableProps } 
                         { ...provided.dragHandleProps }>
                    <Card style={ styles.container }>
                        <Typography gutterBottom>
                            { text }
                        </Typography>
                    </Card>
                </div>
                ) 
            }
        </Draggable>
    )
}

const styles = {
    container: {
        marginBottom: "5px"
    }
}

export default TrelloCard;