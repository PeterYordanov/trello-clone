import React from "react";
import TrelloCard from "./TrelloCard";
import TrelloActionButton from "./TrelloActionButton";
import { Droppable } from "react-beautiful-dnd";

const TrelloList = ({ title, cards, listID }) => {
    return(
        <Droppable droppableId={ String(listID) }>
            { provided => ( 
                <div { ...provided.droppableProps } ref={ provided.innerRef } style={ styles.container }>
                    <h4>{ title }</h4>
                    { cards.map((card, index) => (<TrelloCard key={ card.id } text={ card.text } index={ index } id={ card.id } />)) }
                    { provided.placeholder }
                    <TrelloActionButton listID={ listID } />
                </div>
             ) }
        </Droppable>
    )
}

const styles = {
    container: {
        backgroundColor: "#dfe3e6",
        borderRadius: 3,
        width: "15%",
        height: "100%",
        padding: "5px",
        marginRight: 10
    }
}

export default TrelloList;